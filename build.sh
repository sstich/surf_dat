#!/bin/bash

gcloud beta compute --project "ai-for-fun-and-profit" scp "./auth.json" --zone "us-west2-a" "surf-dat-2:~/surf_dat/"
git push origin master
gcloud beta compute --project "ai-for-fun-and-profit" ssh --zone "us-west2-a" "surf-dat-2"
