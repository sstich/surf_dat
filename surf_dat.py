import pytz
import calendar
from datetime import datetime, timedelta
import requests
from airtable import Airtable
import string
from twilio.rest import Client
import json
import os


# load auth / configuration from auth.json
with open(os.environ['SURF_DAT_AUTH_PATH']) as stream:
    env = os.environ['SURF_DAT_ENVIRONMENT']
    auth_envs = json.load(stream)
    auth = auth_envs[env]

class SurfData(object):
    def __init__(self, api_key, spot_id=255):
        self.api_key = api_key
        self.spot_id = spot_id
        self._data = None

    @property
    def data(self):
        if self._data is None:
            raise Exception("Must call get_forecasts first")
        else:
            return self._data

    def get_forecasts(self):
        url = f'http://magicseaweed.com/api/{self.api_key}/forecast?spot_id={self.spot_id}&fields=timestamp,localTimestamp,issueTimestamp,fadedRating,solidRating,swell.*,wind.*&units=us'
        resp = requests.get(url)
        resp.raise_for_status()
        self._data = resp.json()
        return self.data

    def find_actionable_forecast(self):
        forecasts = self.data
        actionable_forecasts = []
        for forecast in forecasts:
            if self.is_surf_good(forecast) and self.is_time_right(forecast):
                actionable_forecasts.append(forecast)

        if len(actionable_forecasts) > 0:
            return self.format_forecast(actionable_forecasts[0]) # return first=soonest one
        else:
            return None

    @staticmethod
    def get_localtime(forecast):
        # The docs say this is wrong, but it seems to be right...
        # return datetime.fromtimestamp(forecast['timestamp'])
        return datetime.fromtimestamp(forecast['localTimestamp'])

    @staticmethod
    def format_hour(forecast):
        hour = SurfData.get_localtime(forecast).hour
        if hour == 0:
            return 'midnight'
        elif hour == 12:
            return 'noon'
        elif hour < 12:
            return f'{hour}am'
        else:
            return f'{hour-12}pm'

    @staticmethod
    def format_wind(forecast):
        wind_direction = forecast['wind']['direction']
        wind_speed = forecast['wind']['speed']
        if (wind_direction >= 215) and (wind_direction <= 325):
            return f"offshore at {wind_speed}mph"
        else:
            return f"{wind_speed}mph"

    @staticmethod
    def format_forecast(forecast):
        day_of_week = calendar.day_name[SurfData.get_localtime(forecast).weekday()]
        hour = SurfData.format_hour(forecast)
        min_height = forecast['swell']['minBreakingHeight']
        max_height = forecast['swell']['maxBreakingHeight']
        period = forecast['swell']['components']['primary']['period']
        wind_direction = forecast['wind']['direction']
        wind_speed =  forecast['wind']['speed']
        wind_string = SurfData.format_wind(forecast)

        body = f"Surf's good this {day_of_week}, {hour}: " \
               f"{min_height}–{max_height}ft at {period}s, " \
               f"winds {wind_string}." \
                "\nhttps://magicseaweed.com/Ocean-Beach-Surf-Report/255/" \
                "\nhttps://www.surfline.com/surf-report/south-ocean-beach/5842041f4e65fad6a77087f9"

        return body

    @staticmethod
    def is_surf_good(forecast):
        # good surf has a rating above 4.0
        if forecast['solidRating'] < 4:
            return False

        # automatically return true if 5.0 == epic
        if forecast['solidRatnig'] == 5:
            return True

        # good surf comes with low wind, or offshore wind
        wind_direction = forecast['wind']['direction']
        wind_speed = forecast['wind']['speed']
        if wind_speed > 5:
            if (wind_direction < 215) or (wind_direction > 325):
                return False # wind strong and not offshore

        # good surf has long period
        period = forecast['swell']['components']['primary']['period']
        if period < 10:
            return False

        return True

    @staticmethod
    def is_time_right(forecast):
        tz = pytz.timezone('America/Los_Angeles')
        california_now = datetime.now(tz).replace(tzinfo=None)
        forecast_time = SurfData.get_localtime(forecast)
        if (forecast_time < california_now):
            return False
        if (forecast_time - california_now) < timedelta(days=4): # next 4 days
            if forecast_time.hour in [6,9,12,15,18]: # daylight hours
                return True
            return False
        else:
            return False

def send_to_distribution_list(actionable_forecast):
    airtable = Airtable(
        auth['airtable']['base'],
        auth['airtable']['table'],
        api_key=auth['airtable']['api_key']
    )
    phone_numbers = [x['fields']['phone_number'] for x in airtable.get_all()]

    account_sid = auth['twilio']['account_sid']
    auth_token  = auth['twilio']['auth_token']
    client = Client(account_sid, auth_token)

    for phone_number in phone_numbers:
        formatted_number = phone_number\
                          .replace('(', '')\
                          .replace(')', '')\
                          .replace(' ', '')\
                          .replace('-', '')
        message = client.messages.create(
            to=formatted_number,
            from_=auth['twilio']['from_number'],
            body=actionable_forecast
        )

if __name__ == '__main__':
    sd = SurfData(auth['magic_seaweed']['key'])
    sd.get_forecasts()
    actionable_forecast = sd.find_actionable_forecast()
    if actionable_forecast is not None:
        send_to_distribution_list(actionable_forecast)
    else:
        print("No good surf")

